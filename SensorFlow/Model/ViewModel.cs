using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBLibrary.Business;

namespace SensorFlow.Model
{
    public class DataCapteurSynth
    {
        public int IdCapteur { get; set; }

        public int CountTotalData { get; set; }

        public DateTime FirstReleveDateTime { get; set; }

        public DateTime LastReleveDateTime { get; set; }

        public int CountTotalReleve { get; set; }

        public int DeltaTimeReleve { get; set; }

        public decimal TempMoyenne { get; set; }

        public decimal TempMin { get; set; }

        public decimal TempMax { get; set; }

        public decimal HygroMin { get; set; }

        public decimal HygroMax { get; set; }

        public decimal HygroMoyenne { get; set; }   
    }
}
