using System;
using System.IO;

namespace SensorFLow
{
    public class AnalyseLigne
    {
        public AnalyseLigne(string line)
        {
            try
            {
                int id;
                DateTime dateTime;
                double temperature;
                double hygrometrie;

                string[] splited = line.Split(' ');
                splited[0] = splited[0].TrimEnd('.');
                int.TryParse(splited[0], out id);

                DateTime.TryParse($"{splited[1]} {splited[2]}", out dateTime);

                double.TryParse(splited[3].Replace('.', ','), out temperature);

                double.TryParse(splited[4].TrimEnd('%').Replace('.', ','), out hygrometrie);

                this.Id = id;
                this.DateTime = dateTime;
                this.Temperature = temperature;
                this.Hygrometrie = hygrometrie;
            }
            catch (Exception)
            {
                throw new FileLoadException("Parsing error");
            }
        }

        public int Id { get; set; }

        public DateTime DateTime { get; set; }

        public double Temperature { get; set; }

        public double Hygrometrie { get; set; }

        public override string ToString()
        {
            return $"{nameof(Id)}: {Id}, {nameof(DateTime)}: {DateTime}, {nameof(Temperature)}: {Temperature}, {nameof(Hygrometrie)}: {Hygrometrie}";
        }
    }
}
