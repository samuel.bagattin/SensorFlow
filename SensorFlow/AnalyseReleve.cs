using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SensorFLow;

namespace SensorFlow
{
    class AnalyseReleve
    {
        public AnalyseReleve(int idReleve, DateTime dateTime, List<AnalyseLigne> listLigne)
        {
            IdReleve = idReleve;
            DateTime = dateTime;
            ListLigne = listLigne;
        }

        public int IdReleve { get; set; }

        public DateTime DateTime { get; set; }

        public List<AnalyseLigne> ListLigne { get; set; }
    }
}
