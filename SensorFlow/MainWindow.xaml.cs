using SensorFlow.Model;

namespace SensorFLow
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration;
    using System.IO;
    using System.Windows;
    using System.Windows.Controls;
    using DBLibrary.Business;
    using DBLibrary.DataAccess;
    using DBLibrary.Service;
    using Microsoft.Win32;
    using SensorFlow;
    using Ligne = DBLibrary.Business.Ligne;

    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            List<DataCapteurSynth> listSynth = new List<DataCapteurSynth>();
            listSynth.Add(new DataCapteurSynth{ TempMoyenne = 50});
            DataGridCapteurSynth.ItemsSource = listSynth;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.FileName = "Document"; // Default file name
            dlg.DefaultExt = ".txt"; // Default file extension
            dlg.Filter = "Fichier de données capteur|*.txt"; // Filter files by extension

            var result = dlg.ShowDialog();

            if (result == true)
            {
                string pathToFile = dlg.FileName;
                string filename = dlg.SafeFileName;

                int id_capteur = 0;
                int id_releve = 0;
                string name_capteur = null;

                bool parsingFileName = true;

                try
                {
                    int.TryParse(filename.Split('.')[0].Split('_')[0], out id_capteur);

                    name_capteur = filename.Split('.')[0].Split('_')[1];

                    int.TryParse(filename.Split('.')[0].Split('_')[2], out id_releve);
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Nom du fichier incorrect");
                    parsingFileName = false;
                }

                if (parsingFileName)
                {
                    string line;

                    StreamReader file = new StreamReader(pathToFile);

                    bool parsing = true;


                    List<AnalyseLigne> analysesLignes = new List<AnalyseLigne>();

                    while ((line = file.ReadLine()) != null)
                    {
                        try
                        {
                            analysesLignes.Add(new AnalyseLigne(line));
                        }
                        catch (FileLoadException value)
                        {
                            MessageBox.Show("Fichier erroné");
                            parsing = false;
                        }
                    }

                    // BESOIN DE VERIFIER SI UN CAPTEUR EXISTE DEJA AVEC DU LINQ
                    if (parsing)
                    {
                        Capteur checkCapteur = new ServiceCapteur().Get(id_capteur);
                        Capteur newCapteur = new Capteur() { IdCapteur = id_capteur };
                        if (checkCapteur == null)
                        {
                            newCapteur = new ServiceCapteur().Add(new Capteur(id_capteur, name_capteur));
                        }

                        Releve newReleve = new ServiceReleve().Add(new Releve(id_releve, DateTime.Now, newCapteur.IdCapteur));

                        foreach (var ligneInsert in analysesLignes)
                        {
                            Ligne newLigne = new ServiceLigne().Add(new Ligne(ligneInsert.Id, ligneInsert.DateTime, newReleve.IdReleve));
                            Data newTemp = new ServiceData().Add(new Data(ligneInsert.Temperature, 1, newLigne.IdLigne));
                            Data newHygro = new ServiceData().Add(new Data(ligneInsert.Hygrometrie, 2, newLigne.IdLigne));
                            Console.WriteLine(newTemp);
                            Console.WriteLine(newHygro);
                        }
                    }

                    file.Close();
                }
            }
        }

        private void ButtonError_OnClick(object sender, RoutedEventArgs e)
        {
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
