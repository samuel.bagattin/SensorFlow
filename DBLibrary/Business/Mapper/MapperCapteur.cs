using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBLibrary.DataAccess;

namespace DBLibrary.Business.Mapper
{
    static class MapperCapteur
    {
        public static DataAccess.capteur Map(Capteur capteur)
        {
            return new capteur()
            {
                id_capteur = capteur.IdCapteur,
                name_capteur = capteur.NameCapteur,
            };
        }

        public static Capteur Map(DataAccess.capteur capteur)
        {
            return new Capteur(capteur.id_capteur, capteur.name_capteur);
        }

        public static List<Capteur> Map(List<DataAccess.capteur> capteurs)
        {
            return (from v in capteurs select Map(v)).ToList();
        }
    }
}
