using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBLibrary.DataAccess;

namespace DBLibrary.Business.Mapper
{
    static class MapperReleve
    {
        public static DataAccess.releve Map(Releve releve)
        {
            return new releve()
            {
                date_releve = releve.DateReleve,
                id_releve = releve.IdReleve,
                id_capteur = releve.IdCapteur
            };
        }

        public static Releve Map(DataAccess.releve releve)
        {
            return new Releve(releve.id_releve, releve.date_releve, releve.id_capteur);
        }

        public static List<Releve> Map(List<DataAccess.releve> releves)
        {
            return (from v in releves select Map(v)).ToList();
        }
    }
}
