using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBLibrary.DataAccess;

namespace DBLibrary.Business.Mapper
{
    static class MapperData
    {
        public static DataAccess.data Map(Data data)
        {
            return new data()
            {
                id_data = data.IdData,
                id_ligne = data.IdLigne,
                id_meta = data.IdMeta,
                value_data = data.ValueData
            };
        }

        public static Data Map(DataAccess.data data)
        {
            return new Data(data.value_data, data.id_meta, data.id_ligne){ IdData = data.id_data };
        }
    }
}
