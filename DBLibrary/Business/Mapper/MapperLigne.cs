using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBLibrary.Business.Mapper
{
    static class MapperLigne
    {
        public static DataAccess.Ligne Map(Ligne ligne)
        {
            return new DataAccess.Ligne()
            {
                id_ligne = ligne.IdLigne,
                number_ligne = ligne.NumberLigne,
                date_ligne = ligne.DateLigne,
                id_releve = ligne.IdReleve
            };
        }

        public static Ligne Map(DataAccess.Ligne ligne)
        {
            return new Ligne(ligne.number_ligne, ligne.date_ligne, ligne.id_releve);
        }

        public static List<Ligne> Map(List<DataAccess.Ligne> lignes)
        {
            return (from v in lignes select Map(v)).ToList();
        }
    }
}
