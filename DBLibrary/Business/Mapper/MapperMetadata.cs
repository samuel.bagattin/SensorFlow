using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBLibrary.Business.Mapper
{
    static class MapperMetadata
    {
        public static Metadata Map(DataAccess.metadata metadata)
        {
            return new Metadata(metadata.id_meta, metadata.name_meta);
        }
    }
}
