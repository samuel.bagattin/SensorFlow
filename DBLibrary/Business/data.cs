using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBLibrary.Business
{
    public class Data
    {
        public Data(double valueData, int idMeta, int idLigne)
        {
            ValueData = valueData;
            IdMeta = idMeta;
            IdLigne = idLigne;
        }

        public override string ToString()
        {
            return $"{nameof(IdData)}: {IdData}, {nameof(ValueData)}: {ValueData}, {nameof(IdMeta)}: {IdMeta}, {nameof(IdLigne)}: {IdLigne}";
        }

        public int IdData { get; set; }

        public double ValueData { get; set; }

        public int IdMeta { get; set; }

        public int IdLigne { get; set; }

    }
}
