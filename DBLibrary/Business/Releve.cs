using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBLibrary.Business
{
    public class Releve
    {
        public Releve() { }

        public Releve(int idReleve, DateTime dateReleve, int idCapteur)
        {
            IdReleve = idReleve;
            DateReleve = dateReleve;
            IdCapteur = idCapteur;
        }

        public int IdReleve { get; set; }

        public DateTime DateReleve { get; set; }

        public int IdCapteur { get; set; }
    }
}
