using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBLibrary.Business
{
    public class Metadata
    {
        public Metadata(int idMeta, string nameMeta)
        {
            IdMeta = idMeta;
            NameMeta = nameMeta;
        }

        public int IdMeta { get; set; }

        public string NameMeta { get; set; }

    }
}
