using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBLibrary.Business
{
    public class Ligne
    {
        public Ligne() { }

        public Ligne(int numberLigne, DateTime dateLigne, int idReleve)
        {
            this.NumberLigne = numberLigne;
            this.DateLigne = dateLigne;
            this.IdReleve = idReleve;
        }

        public int IdLigne { get; set; }

        public int NumberLigne { get; set; }

        public DateTime DateLigne { get; set; }

        public int IdReleve { get; set; }
    }
}
