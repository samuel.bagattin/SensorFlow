using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBLibrary.Business
{
    public class Capteur
    {
        public Capteur() { }

        public Capteur(int idCapteur, string nameCapteur)
        {
            this.IdCapteur = idCapteur;
            this.NameCapteur = nameCapteur;
            this.Releve = new HashSet<Releve>();
        }

        public int IdCapteur { get; set; }

        public string NameCapteur { get; set; }

        public ICollection<Releve> Releve { get; set; }
    }
}
