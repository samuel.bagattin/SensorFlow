using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBLibrary.Business.Mapper;
using DBLibrary.DataAccess;

namespace DBLibrary.Service
{
    public class ServiceLigne
    {

        public ServiceLigne()
        {
            _ctx = new SensorFLowEntities();
        }

        private SensorFLowEntities _ctx { get; set; }

        public List<Business.Ligne> Get()
        {
            return (from a in _ctx.Ligne select MapperLigne.Map(a)).ToList();
        }

        public Business.Ligne Get(int id)
        {
            return MapperLigne.Map((from a in _ctx.Ligne where id == a.id_ligne select a).FirstOrDefault());
        }

        public Business.Ligne Add(Business.Ligne ligne)
        {
            var entity = new DataAccess.Ligne() { id_ligne = ligne.IdLigne, number_ligne = ligne.NumberLigne, date_ligne = ligne.DateLigne, id_releve = ligne.IdReleve};

            _ctx.Ligne.Add(entity);
            _ctx.SaveChanges();

            ligne.IdLigne = entity.id_ligne;
            return ligne;
        }

    }
}
