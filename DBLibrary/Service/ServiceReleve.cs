using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBLibrary.Business.Mapper;
using DBLibrary.DataAccess;

namespace DBLibrary.Service
{
    public class ServiceReleve
    {

        public ServiceReleve()
        {
            _ctx = new SensorFLowEntities();
        }

        private SensorFLowEntities _ctx { get; set; }

        public List<Business.Releve> Get()
        {
            return (from a in _ctx.releve select MapperReleve.Map(a)).ToList();
        }

        public Business.Releve Get(int id)
        {
            return MapperReleve.Map((from a in _ctx.releve where id == a.id_releve select a).FirstOrDefault());
        }

        public Business.Releve Add(Business.Releve releve)
        {
            var entity = new DataAccess.releve() { id_releve = releve.IdReleve, id_capteur = releve.IdCapteur, date_releve = releve.DateReleve};

            _ctx.releve.Add(entity);
            _ctx.SaveChanges();

            releve.IdReleve = entity.id_releve;
            return releve;
        }
    }
}
