using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBLibrary.Business.Mapper;
using DBLibrary.DataAccess;

namespace DBLibrary.Service
{
    public class ServiceData
    {

        public ServiceData()
        {
            _ctx = new SensorFLowEntities();
        }

        private SensorFLowEntities _ctx { get; set; }

        public List<Business.Data> Get()
        {
            return (from a in _ctx.data select MapperData.Map(a)).ToList();
        }

        public Business.Data Get(int id)
        {
            return MapperData.Map((from a in _ctx.data where id == a.id_data select a).FirstOrDefault());
        }

        public Business.Data Add(Business.Data data)
        {
            var entity = new DataAccess.data() { id_data = data.IdData, value_data = data.ValueData, id_meta = data.IdMeta, id_ligne = data.IdLigne};

            _ctx.data.Add(entity);
            _ctx.SaveChanges();

            data.IdData = entity.id_data;
            return data;
        }
    }
}
