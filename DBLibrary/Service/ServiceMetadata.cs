using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBLibrary.Business.Mapper;
using DBLibrary.DataAccess;

namespace DBLibrary.Service
{
    public class ServiceMetadata
    {

        public ServiceMetadata()
        {
            _ctx = new SensorFLowEntities();
        }

        private SensorFLowEntities _ctx { get; set; }

        public List<Business.Metadata> Get()
        {
            return (from a in _ctx.metadata select MapperMetadata.Map(a)).ToList();
        }

        public Business.Metadata Get(int id)
        {
            return MapperMetadata.Map((from a in _ctx.metadata where id == a.id_meta select a).FirstOrDefault());
        }

        public Business.Metadata Add(Business.Metadata metadata)
        {
            var entity = new DataAccess.metadata() { id_meta = metadata.IdMeta, name_meta = metadata.NameMeta };

            _ctx.metadata.Add(entity);
            _ctx.SaveChanges();

            metadata.IdMeta = entity.id_meta;
            return metadata;
        }
    }
}
