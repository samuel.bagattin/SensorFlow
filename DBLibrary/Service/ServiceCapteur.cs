using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBLibrary.Business;
using DBLibrary.Business.Mapper;
using DBLibrary.DataAccess;

namespace DBLibrary.Service
{
    public class ServiceCapteur
    {
        public ServiceCapteur()
        {
            _ctx = new SensorFLowEntities();
        }

        private SensorFLowEntities _ctx { get; set; }

        public List<Business.Capteur> Get()
        {
            return (from a in _ctx.capteur select MapperCapteur.Map(a)).ToList();
        }

        public Business.Capteur Get(int id)
        {
            return MapperCapteur.Map((from a in _ctx.capteur where id == a.id_capteur select a).FirstOrDefault());
        }

        public Business.Capteur Add(Business.Capteur capteur)
        {
            var entity = new DataAccess.capteur() { id_capteur = capteur.IdCapteur, name_capteur = capteur.NameCapteur};

            _ctx.capteur.Add(entity);
            _ctx.SaveChanges();

            capteur.IdCapteur = entity.id_capteur;
            return capteur;
        }
    }
}
